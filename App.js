import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  Image
} from 'react-native';

const App = () => {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View>
          <Text style={styles.title}>Digital Approval</Text>
          <Image style={styles.logo} source={require('./assets/toyota.png')} />
          <TextInput
            style={styles.inputemail}
            placeholder="Alamat Email"
          />
          <TextInput
            style={styles.inputpass}
            placeholder="Password"
          />
          <Text
            style={styles.reset}
            onPress={() => Alert.alert("Reset password pressed")}>
            Reset Password
          </Text>
          <TouchableOpacity
            style={styles.login}
            onPress={() => Alert.alert("Login button pressed")}
          >
            <Text style={styles.loginTitle}>LOGIN</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    backgroundColor: '#fff',
    marginRight: 35,
    marginLeft: 35,
    paddingBottom: 16,
    borderRadius: 10,
  },
  title: {
    textAlign: 'center',
    alignSelf: 'center',
    paddingTop: 7,
    paddingBottom: 7,
    paddingRight: 35,
    paddingLeft: 35,
    backgroundColor: '#002558',
    borderRadius: 100,
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: -20
  },
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#F7F7F7"
  },
  login: {
    alignSelf: 'center',
    backgroundColor: '#287AE5',
    height: 48,
    width: 288,
    marginTop: 20,
    justifyContent: 'center',
    borderRadius: 5,
  },
  loginTitle: {
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
    fontSize: 20,
  },
  inputemail: {
    height: 48,
    width: 288,
    borderWidth: 0.8,
    padding: 10,
    alignSelf: 'center',
    borderColor: '#BFBFBF',
    borderRadius: 5,
    marginBottom: 24,
    fontSize: 17,
  },
  inputpass: {
    height: 48,
    width: 288,
    borderWidth: 0.8,
    padding: 10,
    alignSelf: 'center',
    borderColor: '#BFBFBF',
    borderRadius: 5,
    fontSize: 17,
    marginBottom: 16,
  },
  reset: {
    color: '#287AE5',
    fontStyle: 'italic',
    fontSize: 17,
    textAlign: 'right',
    marginRight: 19,
    marginBottom: 5,
  },
  logo: {
    alignSelf: 'center',
    marginBottom: 38,
    marginTop: 12,
  }
});

export default App;
